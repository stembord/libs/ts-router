import url = require("url");
import { Layer } from "./Layer";
import { Middleware } from "./Middleware";
import { Route } from "./Route";
import { Router } from "./Router";

export interface IParams {
  [key: string]: string;
}

export type IContext<T = { [key: string]: any }> = {
  url: url.UrlWithParsedQuery;
  pathname: string;
  params: IParams;
  route?: Route;
  router?: Router;
  middleware?: Middleware;
  layer?: Layer;
  resolved(): boolean;
  end(): IContext;
} & T;

export const createContext = (url: url.UrlWithParsedQuery): IContext => {
  let _resolved = false;

  const end = () => {
    _resolved = true;
    return context;
  };

  const resolved = () => _resolved;

  const context: IContext = {
    url,
    end,
    resolved,
    pathname: url.pathname || "/",
    query: url.query,
    params: { ...(url.query as IParams) }
  };

  return context;
};
