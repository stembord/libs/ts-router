import * as tape from "tape";
import url = require("url");
import { createContext, IContext, Router } from "./";

tape("Router.handle(context: IContext)", async (assert: tape.Test) => {
  const router = new Router();

  let calledMiddleware = false,
    calledScopedRoute = false,
    calledRoute = false,
    calledAsync = false,
    called404 = false;

  const reset = () => {
    calledMiddleware = false;
    calledScopedRoute = false;
    calledRoute = false;
    calledAsync = false;
    called404 = false;
  };

  router.use(
    "/",
    (context: IContext): Promise<IContext> => {
      calledAsync = false;
      return new Promise((resolve: (context: IContext) => void) => {
        setTimeout(() => {
          calledAsync = true;
          resolve(context);
        }, 0);
      });
    },
    (): void => {
      assert.equals(calledAsync, true, "async should be called");
      calledMiddleware = true;
    }
  );

  const route = router.route(
    "/parent/:parent_id{[0-9]+}(.:format)",
    (): void => {
      assert.equals(calledAsync, true, "async should be called");
    },
    (): void => {
      assert.equals(calledAsync, true, "async should be called");
      calledRoute = true;
    }
  );

  const scope = router.scope("/parent/:parent_id{[0-9]+}");
  const scopeRoute = scope.route(
    "/child/:id{[0-9]+}(.:format)",
    (context: IContext): void => {
      const { parent_id, id } = context.params;

      assert.equals(calledAsync, true, "async should be called");

      if (parent_id && id) {
        assert.equal(+parent_id, 1, "parent_id param should be parsed");
        assert.equal(+id, 1, "id param should be parsed");
      }

      calledScopedRoute = true;
    }
  );
  const grandScopeRoute = scope.route(
    "/child/:child_id{[0-9]+}/grand-child/:id{[0-9]+}(.:format)",
    (context: IContext): void => {
      const { parent_id, child_id, id } = context.params;

      assert.equals(calledAsync, true, "async should be called");

      if (parent_id && child_id && id) {
        assert.equal(+parent_id, 1, "parent_id param should be parsed");
        assert.equal(+child_id, 1, "child_id param should be parsed");
        assert.equal(+id, 1, "id param should be parsed");
      }

      calledScopedRoute = true;
    }
  );

  assert.equals(router.getRoot(), router);
  assert.equals(scope.getRoot(), router);

  router.use("/", (context: IContext): void => {
    assert.equals(calledAsync, true, "async should be called");

    called404 = true;

    if (!context.resolved()) {
      throw new Error("404 - Not Found");
    }
  });

  assert.equal(route.format(1, ""), "/parent/1");
  assert.equal(route.format(1, ".json"), "/parent/1.json");

  assert.equal(scopeRoute.format(1, 1, ""), "/parent/1/child/1");
  assert.equal(scopeRoute.format(1, 1, ".json"), "/parent/1/child/1.json");

  assert.equal(
    grandScopeRoute.format(1, 1, 1, ""),
    "/parent/1/child/1/grand-child/1"
  );
  assert.equal(
    grandScopeRoute.format(1, 1, 1, ".json"),
    "/parent/1/child/1/grand-child/1.json"
  );

  await router.handle(
    createContext(
      url.parse(
        "http://localhost:9999/parent/1/child/1/grand-child/1.json",
        true
      )
    )
  );
  assert.equal(calledMiddleware, true, "middleware should be called");
  assert.equal(calledScopedRoute, true, "scoped route should be called");
  assert.equal(calledRoute, false, "route should not be called");
  assert.equal(called404, false, "404 middleware should not be called");

  reset();

  await router.handle(
    createContext(
      url.parse("http://localhost:9999/parent/1/child/1.json", true)
    )
  );
  assert.equal(calledMiddleware, true, "middleware should be called");
  assert.equal(calledScopedRoute, true, "scoped route should be called");
  assert.equal(calledRoute, false, "route should not be called");
  assert.equal(called404, false, "404 middleware should not be called");

  reset();

  await router.handle(
    createContext(url.parse("http://localhost:9999/parent/1.json", true))
  );
  assert.equal(calledMiddleware, true, "middleware should be called");
  assert.equal(calledScopedRoute, false, "scoped route should not be called");
  assert.equal(calledRoute, true, "route should be called");
  assert.equal(called404, false, "404 middleware should not be called");

  await router
    .handle(createContext(url.parse("http://localhost:9999/not-found", true)))
    .catch(error => {
      assert.equal(calledMiddleware, true, "middleware should be called");
      assert.equal(calledScopedRoute, false, "scope should not be called");
      assert.equal(calledRoute, true, "route should not be called");
      assert.equal(calledAsync, true, "async should be called");
      assert.equal(called404, true, "404 middleware should be called");

      assert.equal(
        error.message,
        "404 - Not Found",
        "404 should be called if no route handled request"
      );
    });

  assert.end();
});
