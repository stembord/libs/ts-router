import { IContext } from "../context";

export const promisifyResult = (
  context: IContext,
  result: Promise<IContext> | IContext | void
): Promise<IContext> => {
  if (result instanceof Promise) {
    return result.then(() => context);
  } else {
    return Promise.resolve(context);
  }
};
