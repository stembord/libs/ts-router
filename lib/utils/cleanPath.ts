export const cleanPath = (path?: string): string => {
  if (!path || path === "/") {
    return "/";
  } else {
    if (path.charAt(0) !== "/") {
      path = "/" + path;
    }
    if (path.charAt(path.length - 1) === "/") {
      path = path.slice(0, -1);
    }

    return path;
  }
};
