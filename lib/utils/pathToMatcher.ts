const rePartsMatcher = /\.[a-zA-Z0-9-_]+|\.\:[a-zA-Z0-9-_]+|\/+[a-zA-Z0-9-_]+|\/\:[a-zA-Z0-9-_]+({.+?})?|\:[a-zA-Z0-9-_]+({.+?})?|\(.+?\)/g,
  rePartMatcher = /\:?[a-zA-Z0-9-_]+|{.+?}/g,
  rePartReplacer = /[\(\)]|\{.+?\}/g;

export class Param {
  name: string;
  regExp: RegExp;
  required: boolean;

  constructor(name: string, regExp: RegExp, required: boolean) {
    this.name = name;
    this.regExp = regExp;
    this.required = required;
  }

  toJSON(json: any = {}) {
    json.name = this.name;
    json.regExp = this.regExp;
    json.required = this.required;
    return json;
  }
}

export interface IMatcher {
  regExp: RegExp;
  params: Param[];
}

export const pathToMatcher = (path: string, end: boolean = true): IMatcher => {
  const parts = (path + "").match(rePartsMatcher) || [],
    params: Param[] = [];

  let pattern = "^";

  parts.forEach(part => {
    if (part.length !== 0) {
      if (part[0] === "(") {
        if (part[1] === "/" || part[1] === ".") {
          pattern += "(?:\\" + part[1];
        }
        const subParts = part.match(rePartMatcher) || [];

        part = subParts[0];

        if (part[0] === ":") {
          let subRegExp = subParts[1];

          subRegExp = subRegExp ? subRegExp.slice(1, -1) : "[a-zA-Z0-9-_]+";

          pattern += "(" + subRegExp + "?)";
          params.push(new Param(part.slice(1), new RegExp(subRegExp), false));
        } else {
          pattern += part;
        }

        pattern += ")?";
      } else {
        if (part[0] === "/" || part[0] === ".") {
          pattern += "\\" + part[0] + "+";
        }
        const subParts = part.match(rePartMatcher) || [];

        part = subParts[0];

        if (part[0] === ":") {
          let subRegExp = subParts[1];

          subRegExp = subRegExp ? subRegExp.slice(1, -1) : "[a-zA-Z0-9\\-_]+";

          pattern += "(" + subRegExp + ")";
          params.push(new Param(part.slice(1), new RegExp(subRegExp), true));
        } else {
          pattern += part;
        }
      }
    }
  });

  if (end === true) {
    pattern += "\\/?$";
  } else {
    pattern += "(?=\\/|$)";
  }

  return {
    regExp: new RegExp(pattern, "i"),
    params
  };
};

export const formatPath = (path: string) => {
  return (path.match(rePartsMatcher) || []).reduce((fmt, part) => {
    if (part) {
      const isOptional = part.charAt(0) === "(";

      part = part.replace(rePartReplacer, "");

      if (isOptional) {
        part = part.slice(1);
      }

      if (part.charAt(1) === ":") {
        fmt += part.charAt(0) + "%s";
      } else if (part.charAt(0) === ":") {
        fmt += "%s";
      } else {
        fmt += part;
      }
    }
    return fmt;
  }, "");
};
