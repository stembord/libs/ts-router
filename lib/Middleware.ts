import { none, Option } from "@stembord/core";
import { IContext } from "./context";
import { IHandler } from "./IHandler";
import { IMiddleware } from "./IMiddleware";
import { Layer } from "./Layer";
import { CANCELLED_MESSAGE } from "./utils/CanceledError";
import { createErrorHandler } from "./utils/createErrorHandler";

export type IMiddlewareHandler = (
  error: Error | null,
  context: IContext
) => Promise<IContext>;

export class Middleware extends Layer {
  protected handlers: IMiddlewareHandler[];

  constructor(
    path: string = "/",
    parent: Option<Layer> = none(),
    end: boolean = false
  ) {
    super(path, parent, end);

    this.handlers = [];
  }

  middleware(error: Error | null, context: IContext): Promise<IContext> {
    return this.handlers
      .reduce(
        (promise, handler) =>
          promise.then(
            () => handler(error, context),
            error =>
              error && error.message === CANCELLED_MESSAGE
                ? Promise.reject(error)
                : handler(error, context)
          ),
        error ? Promise.reject(error) : Promise.resolve(context)
      )
      .then(context => {
        if (this.isEnd()) {
          context.end();
        }
        return context;
      });
  }

  mount(...handlers: Array<IHandler | IMiddleware>): Middleware {
    handlers.reduce((handlers, handler) => {
      handlers.push(createErrorHandler(handler));
      return handlers;
    }, this.handlers);
    return this;
  }

  clear(): Middleware {
    this.handlers.length = 0;
    return this;
  }
}
